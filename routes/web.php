<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profil', 'UserController@show')->name('profil');
Route::patch('/profil/update/{user}', 'UserController@update')->name('profil.update');

Route::get('/user', 'UserController@index')->name('user');
Route::post('/user/tambah', 'UserController@store')->name('user.tambah');
Route::delete('/user/hapus/{user}', 'UserController@destroy')->name('user.hapus');
Route::get('/user/ubah/{user}', 'UserController@edit')->name('user.ubah');
