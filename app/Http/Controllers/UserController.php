<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Hash;
class UserController extends Controller
{
    public function index()
    {
        $users = User::latest()->get();
        return view('user.index', compact('users'));
    }

    public function store(Request $request)
    {
        User::create([
            'name' => $request->name,
            'username' => $request->username,
            'level' => $request->level,
            'password' => Hash::make($request->password),
        ]);
        
        return redirect()->back();
        
    }

    public function edit(User $user)
    {
        return view('user.edit', compact('user'));
    }
    public function show()
    {
        $user = User::where('id', Auth::user()->id)->first();
        return view('profil', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        if ($request->password == '') {
            $user->update([
                'name' => $request->name,
                'level' => $request->level,
            ]);
        } else {
            $user->update([
                'name' => $request->name,
                'password' => Hash::make($request->password),
                'level' => $request->level,
            ]);
        }
        return redirect()->back();
        
    }

    public function destroy(User $user)
    {
        $user->delete();
        
        return redirect()->back();
        
    }
}
