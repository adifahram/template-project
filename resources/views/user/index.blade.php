@extends('layouts.app')
@section('page')
    user
@endsection

@section('content')

<div class="row">
    <div class="col-xs-4">
        <div class="box">
            <div class="box-header">
            <h3 class="box-title">Form user</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form action="{{ route('user.tambah') }}" enctype="multipart/form-data" method="POST" role="form">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="">Nama</label>
                            <input type="text" class="form-control" id="" name="name" placeholder="Masukan Nama">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="">username</label>
                            <input type="text" class="form-control" id="" name="username" placeholder="Masukan username">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="">password</label>
                            <input type="password" class="form-control" id="" name="password" placeholder="Masukan client">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="">Level</label>
                            <select name="level" id="" class="form-control">
                                <option value="user">User</option>
                                <option value="admin">Admin</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-xs-8">
        <div class="box">
            <div class="box-header">
            <h3 class="box-title">Data user</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>no</th>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->username }}</td>
                        <td>
                            <form action="{{ route('user.hapus', $user) }}" method="POST" class="pull-left">
                                @csrf
                                @method('delete')
                                <div class="form-group">
                                    <button type="submit" onclick="return confirm('anda yakin ingin menghapus data?')"  class="btn btn-sm btn-danger">Hapus</button>
                                </div>
                            </form>
                            <a href="{{ route('user.ubah', $user) }}" data-toggle="modal" data-target="#edit" class="btn btn-sm btn-success edit">Ubah</a>    
                            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit">
    <div class="modal-dialog">
    <div class="modal-content">
        
        </div>
    </div>  
</div>
@endsection

@push('script')
<script>
$('.edit').on('click', function(e){
    e.preventDefault();
    $("#edit .modal-content").empty();
    var target = $(this).attr("href");
    $("#edit .modal-content").load(target, function() { 
    $("#edit").modal("show"); 
    });

});
</script>
@endpush