<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ubah data</h4>
    </div>
    <div class="modal-body">
        <form action="{{ route('profil.update',$user) }}" enctype="multipart/form-data" method="POST" role="form">
            @csrf
            @method('patch')
                <div class="box-body">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="">name</label>
                        <input type="text" class="form-control" id="" value="{{ $user->name }}" name="name" required>                            
                    </div>
                    <div class="form-group">
                        <label for="">Username</label>
                        <input type="text" class="form-control" id="" value="{{ $user->username }}" name="email" readonly>                            
                    </div>
                    <div class="form-group">
                        <label for="">password</label>
                        <input type="password" class="form-control" name="password">                            
                    </div>
                    <div class="form-group">
                        <label for="">Level</label>
                        <select name="level" id="" class="form-control">
                            <option @if ($user->level == 'user') selected @endif value="user">User</option>
                            <option @if ($user->level == 'admin') selected @endif value="admin">Admin</option>
                        </select>
                    </div>
                </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
        </form>
    </div>
        