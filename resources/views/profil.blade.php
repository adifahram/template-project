@extends('layouts.app')
@section('page')
    Edit Profil
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
            <h3 class="box-title">Form </h3>
            </div>
            <!-- /.box-header -->
                <form action="{{ route('profil.update', $user) }}" method="POST" role="form">
                    @csrf
                    @method('patch')
                    <div class="box-body">

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="">name</label>
                            <input type="text" class="form-control" id="" value="{{ $user->name }}" name="name" required>                            
                        </div>
                        <div class="form-group">
                            <label for="">email</label>
                            <input type="text" class="form-control" id="" value="{{ $user->username }}" name="email" readonly>                            
                        </div>
                        <div class="form-group">
                            <label for="">password</label>
                            <input type="password" class="form-control" name="password">                            
                        </div>
                    </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
        </div>
    </div>

@endsection