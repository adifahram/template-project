@extends('layouts.app')

@section('content')

<div class="box box-default">
<div class="box-header with-border">
    <h3 class="box-title">Blank Box</h3>
</div>
<div class="box-body">
    The great content goes here
</div>
</div>
<div class="row">
    <div class="col-xs-4">
        <div class="box">
            <div class="box-header">
            <h3 class="box-title">Form tes</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form action="" enctype="multipart/form-data" method="POST" role="form">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="">Code</label>
                            <input type="text" class="form-control" id="" name="code" placeholder="Masukan Code">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="">nama</label>
                            <input type="text" class="form-control" id="" name="name" placeholder="Masukan Nama">
                        </div>
                    </div>
                    
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-xs-8">
        <div class="box">
            <div class="box-header">
            <h3 class="box-title">Data tes</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>no</th>
                        <th>Code</th>
                        <th>Nama</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    {{--  @foreach ($no as $tes)  --}}
                    <tr>
                        <td>tes</td>
                        <td>s</td>
                        <td>s</td>
                        <td>
                            <form action="" method="POST" class="pull-left">
                                @csrf
                                @method('delete')
                                <div class="form-group">
                                    <button type="submit" onclick="return confirm('anda yakin ingin menghapus data?')"  class="btn btn-sm btn-danger">Hapus</button>
                                </div>
                            </form>
                            
                            <a href="" data-toggle="modal" data-target="#edit" class="btn btn-sm btn-success edit">Ubah</a>    
                        </td>
                    </tr>
                    {{--  @endforeach  --}}
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit">
    <div class="modal-dialog">
    <div class="modal-content">
        {{--  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Ubah data</h4>
</div>
<div class="modal-body">
    <form action="{{ route('customer.update',$customer) }}" enctype="multipart/form-data" method="POST" role="form">
        @csrf
        @method('patch')
        <div class="box-body">
            <div class="form-group">
                <label for="">Code</label>
                <input type="text" class="form-control" id="" name="code" value="{{ $customer->code }}" placeholder="Masukan Code">
            </div>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="">name</label>
                <input type="text" class="form-control" id="" value="{{ $customer->name }}" name="name" placeholder="Masukan Name">
            </div>
        </div>
        
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
      --}}
        </div>
    </div>  
</div>
@endsection

@push('script')
<script>
$('.edit').on('click', function(e){
    e.preventDefault();
    $("#edit .modal-content").empty();
    var target = $(this).attr("href");
    $("#edit .modal-content").load(target, function() { 
    $("#edit").modal("show"); 
    });

});
</script>
@endpush
