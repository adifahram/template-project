<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
		  <!-- Sidebar user panel -->
		  <div class="user-panel">
			<div class="pull-left image">
			  <img src="{{ asset('/admin/dist/img/avatar04.png')}}" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
			  <p>nama</p>
			  <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		  </div>
		  <!-- sidebar menu: : style can be found in sidebar.less -->
		  <ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-dashboard"></i> <span>Data Master</span>
					<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href=""><i class="fa fa-circle-o"></i> Aset</a></li>
					<li><a href=""><i class="fa fa-circle-o"></i> Divisi</a></li>
				</ul>
			</li>
		<li><a href="{{ route('user') }}"><i class="fa fa-dashboard"></i> <span>Tambah User</span></a></li>
			
		</ul>
	</section>
	</aside>