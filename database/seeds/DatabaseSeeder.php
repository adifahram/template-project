<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            [
                'name'=>'admin',
                'username'=>'admin',
                'level'=>'admin',
                'password'=>Hash::make('123qwe'),
            ],
            [
                'name'=>'user',
                'username'=>'user',
                'level'=>'userf',
                'password'=>Hash::make('123qwe'),
            ]
       ]);
    }
}
